package task2;

public class StringReverser {

    public static String revertLine(String line){
        if (line.lastIndexOf(" ")<0){
            return " "+line;
        }
        return line.substring(line.lastIndexOf(' '))+revertLine(line.substring(0,line.lastIndexOf(' ')));
    }

    public static void main(String[] args) {
        System.out.println(revertLine("This method is called recursively"));
    }
}
