package task8;

import org.junit.Assert;
import org.junit.Test;

public class PirateChest {

    private static int[] determineCoinsAmount(int lastYear, int coins) {
        int[] years = new int[lastYear];
        int[] answer = new int[2];
        for (int firstYearCoins = 0; firstYearCoins <= coins; firstYearCoins++) {
            for (int secondYearCoins = 0; secondYearCoins < firstYearCoins; secondYearCoins++) {
                years[0] = firstYearCoins;
                years[1] = secondYearCoins;
                for (int i = 2; i < lastYear; i++) {
                    years[i] = years[i - 1] + years[i - 2];
                }
                if (years[lastYear - 1] == coins) {
                    answer[0] = firstYearCoins;
                    answer[1] = firstYearCoins - secondYearCoins;
                    return answer;
                }
            }
        }
        throw new RuntimeException();
    }

    @Test
    public void testDetermineCoinsAmount() {
        Assert.assertArrayEquals(new int[]{5, 3}, determineCoinsAmount(6, 25));
        Assert.assertArrayEquals(new int[]{5, 3}, determineCoinsAmount(5, 16));
        Assert.assertArrayEquals(new int[]{10, 7}, determineCoinsAmount(6, 45));
        Assert.assertArrayEquals(new int[]{17, 3}, determineCoinsAmount(4, 45));
    }

}
