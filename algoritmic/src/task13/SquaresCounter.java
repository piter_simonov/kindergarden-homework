package task13;

public class SquaresCounter {

    public static int calculateSquareCount(int height, int width) {
        if (height == width) {
            return 1;
        } else if (height > width) {
            return 1 + calculateSquareCount(height - width, width);
        } else {
            return 1 + calculateSquareCount(height, width - height);
        }
    }

    public static void main(String[] args) {
        System.out.println(calculateSquareCount(98, 31));
    }
}
