package task4;

public class TowersController {
    public static void solveTowersRecursive(int rings, int source, int destination, int temp) {
        if (rings == 1) {
            System.out.println(source + " --> " + destination);
        } else {
            solveTowersRecursive(rings - 1, source, temp, destination);
            System.out.println(source + " --> " + destination);
            solveTowersRecursive(rings - 1, temp, destination, source);
        }
    }

    public static void main(String[] args) {
        solveTowersRecursive(4, 1, 3, 2);
    }
}
