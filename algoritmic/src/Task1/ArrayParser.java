package Task1;

import java.util.HashSet;

public class ArrayParser {

    public static void determineUniqueSequence(int[] array) {
        HashSet<Integer> currentSequence;
        int maxLength = 0;
        int startIndex = 0;
        int endIndex = 0;
        for (int start = 0; start < array.length; start++) {
            currentSequence = new HashSet<>();
            for (int end = start; end < array.length; end++) {
                if (currentSequence.add(array[end])) {
                    if (maxLength < currentSequence.size()) {
                        maxLength = currentSequence.size();
                        startIndex = start;
                        endIndex = end - 1;
                    }
                } else {
                    break;
                }
            }
        }
        System.out.println("Max length of unique sequence is: " + maxLength +
                ". Start index: " + startIndex + ". End index " + endIndex);
    }

    public static void main(String[] args) {
        determineUniqueSequence(new int[]{1, 2, 4, 2, 5, 6, 7, 4, 5, 6, 6, 6, 3, 9});
    }
}
