package task14;

public class Finder {

    public static int findDigit(int number){
        long count = 0;
        for (int digit = 1; digit<number;digit++){
            count+=digit;
            if (count>=number){
                return digit;
            }
        }
        return 1;
    }

    public static void main(String[] args) {
        System.out.println(findDigit(2002));
    }
}
